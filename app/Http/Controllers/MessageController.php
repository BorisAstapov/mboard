<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Message;

class MessageController extends Controller
{
    public function store(Request $request)
    {        

        $request->validate([
            'name' => 'required|alpha|max:255',
            'email' => 'required|email',
            'message' => 'required',
        ]);

        $message = new Message;
        $message->name = $request->name;
        $message->email = $request->email;
        $message->message = $request->message;
        $message->save();
        return redirect('/');
    }

    public function show()
    {
        $messages = Message::select('name', 'email', 'message', 'created_at')
        ->orderBy('created_at', 'desc')
        ->get();
        return view('board', ['messages' => $messages]);
    }
}
